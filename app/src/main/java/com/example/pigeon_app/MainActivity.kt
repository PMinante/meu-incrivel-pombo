package com.example.pigeon_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.lista.*

const val USER_ID = 0
const val OTHER_ID = 1

class MainActivity : AppCompatActivity() {
    private var fromUser:Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setContentView(R.layout.lista)
        setUpListeners()
        setUpRecyclerView()
    }

    private fun setUpListeners() {
        send_button.setOnClickListener{
            val messageText = message_EditText.text.toString()
            message_EditText.setText("")

            val adapter = message_list.adapter

            if(adapter is MessageAdapter){

                val message = ChatMessage(
                    messageText,
                    if(fromUser) USER_ID else OTHER_ID
                )

                adapter.addItems(message)

                message_list.scrollToPosition(adapter.itemCount - 1)

                fromUser = !fromUser
            }
        }
    }

    private fun setUpRecyclerView() {
        message_list.layoutManager = LinearLayoutManager(this)
        message_list.adapter = MessageAdapter()
        person_list.layoutManager = LinearLayoutManager(this)
        person_list.adapter = ListAdapter()
    }
}